document.addEventListener('DOMContentLoaded', ()=>{
    let movie_item = document.querySelectorAll('.movie_item');


})

function searchForMovie() {
    let titleEl = document.querySelector('#movie-title-search');
    let title = titleEl.value;

    fetch('/api/movies/'+title)
    .then(response => response.json())
    .then(data => {
        let outerDiv = document.querySelector('#movie-search-results');
        outerDiv.innerHTML = '';

        let movies = data.Search;
        movies.map(displayMovieSearch.bind(null, outerDiv));
    });

    return false;
}

function displayMovieSearch(outerDiv, movie) {

    let title = movie.Title;
    let poster = movie.Poster;
    let year = movie.Year;
    let imdb_id = movie.imdbID;

    let use_button = create_use_button(imdb_id);
    outerDiv.appendChild(use_button);

    let poster_div = create_img_poster(poster);
    outerDiv.appendChild(poster_div);

    let title_el = create_title(title);
    outerDiv.appendChild(title_el);

    let year_el = create_year(year);
    outerDiv.appendChild(year_el);
}

function create_use_button(imdb_id) {
    let use_button = document.createElement('button');
    use_button.classList.add('use-movie');
    use_button.classList.add('btn');
    use_button.classList.add('btn-sm');
    use_button.classList.add('btn-primary');
    use_button.dataset.imdb = imdb_id;
    use_button.innerHTML = 'Use';

    use_button.addEventListener('click', ev => {
        fetch('/api/movies/add/' + ev.target.dataset.imdb)
        .then(() => {
            window.location.href = "/";
        })
    })
    return use_button;
}

function create_img_poster(img_src) {
    let poster_div = document.createElement('div');
    poster_div.classList.add('poster');
    let image_el = document.createElement('img');
    if(img_src !== 'N/A'){
        image_el.src=img_src;
    }
    poster_div.appendChild(image_el);
    return poster_div;
}

function create_title(title) {
    let title_el = document.createElement('div');
    title_el.classList.add('title');
    title_el.innerHTML = title;
    return title_el;
}

function create_year(year) {
    let year_el = document.createElement('div');
    year_el.classList.add('year');
    year_el.innerHTML = year;
    return year_el;
}