<?php

namespace App\Interfaces;

interface Service {

    public function getList();
    public function getSingle(int $id);
    public function addSingle($data);
    public function updateSingle(int $id, $data);
    public function deleteSingle(int $id);

}

