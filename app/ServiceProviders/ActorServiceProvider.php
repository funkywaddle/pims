<?php

namespace App\ServiceProviders;

use App\Models\Actor;
use App\Services\ActorService;
use Illuminate\Support\ServiceProvider;

class ActorServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(ActorService::class, function ($app) {
            return new ActorService(new Actor());
        });

        $this->app->when('App\Http\Controllers\ActorController')
            ->needs('App\Interfaces\Service')
            ->give(ActorService::class);
    }
}
