<?php

namespace App\ServiceProviders;

use App\Models\Studio;
use App\Services\StudioService;
use Illuminate\Support\ServiceProvider;

class StudioServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(StudioService::class, function ($app) {
            return new StudioService(new Studio());
        });

        $this->app->when('App\Http\Controllers\StudioController')
            ->needs('App\Interfaces\Service')
            ->give(StudioService::class);
    }
}
