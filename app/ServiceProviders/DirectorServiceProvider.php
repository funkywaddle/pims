<?php

namespace App\ServiceProviders;

use App\Models\Director;
use App\Services\DirectorService;
use Illuminate\Support\ServiceProvider;

class DirectorServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(DirectorService::class, function ($app) {
            return new DirectorService(new Director());
        });

        $this->app->when('App\Http\Controllers\DirectorController')
            ->needs('App\Interfaces\Service')
            ->give(DirectorService::class);
    }
}
