<?php

namespace App\ServiceProviders;

use App\Models\Movie;
use App\Services\MovieService;
use Illuminate\Support\ServiceProvider;

class MovieServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(MovieService::class, function ($app) {
            return new MovieService(new Movie());
        });

        $this->app->when('App\Http\Controllers\MovieController')
            ->needs('App\Interfaces\Service')
            ->give(MovieService::class);
    }
}
