<?php

namespace App\Http\Controllers;

use App\Interfaces\Service;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    private $service;

    public function __construct(Service $service){
        $this->service = $service;
    }

    public function index() {
        $movies = $this->service->getList();

        return view('pims.index', ['movies'=>$movies, 'active'=>$this->_get_default_context()]);
    }

    public function single($id) {
        $movie = $this->service->getSingle($id);

        return view('pims.movie', ['movie'=>$movie]);
    }

    private function _get_default_context() {
        return 'movies';
    }
}
