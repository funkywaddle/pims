<?php

namespace App\Services;

use App\Interfaces\Service;
use App\Models\Movie;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\ModelHelper;

class MovieService implements Service{

    private Model $model;

    public function __construct(Model $model) {
        $this->model = $model;
    }

    public function getList() : LengthAwarePaginator {
        return $this->model->orderBy('title')->paginate(10);
    }

    public function getSingle(int $id) : Movie {
        return $this->model->where('id', $id)->first();
    }

    public function addSingle($data) : void {
        $data = ModelHelper::CleanseData($this->model, $data);
        $model = $this->model->newInstance($data);
        $model->save();
    }

    public function updateSingle(int $id, $data) : void {
        $data = ModelHelper::CleanseData($this->model, $data);
        $model = $this->getSingle($id);
        $model->fill($data);
        $model->save();
    }

    public function deleteSingle(int $id) : void {
        $this->model->destroy($id);
    }
}
