<?php

namespace App\Services;

use App\Interfaces\Service;
use App\Models\Actor;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\ModelHelper;

class ActorService implements Service{

    private Model $model;

    public function __construct(Model $model) {
        $this->model = $model;
    }

    public function getList() : Collection {
        return $this->model->get();
    }

    public function getSingle($id) : Actor {
        return $this->model->where('id', $id)->first();
    }

    public function addSingle($data) : void {
        $data = ModelHelper::CleanseData($this->model, $data);
        $model = $this->model->newInstance($data);
        $model->save();
    }

    public function updateSingle($id, $data) : void {
        $data = ModelHelper::CleanseData($this->model, $data);
        $model = $this->getSingle($id);
        $model->fill($data);
        $model->save();
    }

    public function deleteSingle($id) : void {
        $this->model->destroy($id);
    }
}
