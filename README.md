#P.I.M.S.

This is a Personal Inventory Management System site.
At this time, it is only for movies, but future plans are for TV Seasons, Books, Music, etc

It utilizes a 3rd party api to grab extra data for the movies you are adding to the database.
When you search for a movie, it hits the 3rd party api to search for movies with the title given,
and displays the results, so you can choose which movie you are wanting to add.

Once you click the "Use" button on the chosen movie, it makes another call to the api
to get the extra details (Title, Year, Plot, Actors, Directors, Production Studios, Poster Image, etc)
and adds them to the database with the movie.

On the movie detail view, the actor, director, and studio names are links to Actor, Director, and Studio detail pages
where it shows which movies in the database that respective actor, director, or studio has also performed that same task

I believe this project satisfies the distinct requirements as we never worked with an inventory management system
for any of our projects.

I believe this project satisfies the extra complexity due to having to work with a 3rd party api, environment variable
to store the api key, utilizing a model class not for the purposes of database connectivity, and also utilizing the get_or_create
on a model to retrieve a record from the database if it exists or create one if it doesn't
