@extends("pims.layout")

@section('body')
    <h2>Movie Details</h2>
    <h3 class="movie-title">{{ $movie->title }}</h3>
    <div class="movie-display">
        <img src="{{ $movie->image }}" />
        <div class="movie-details">
            <div class="movie-detail">
                <div class="label">Year:</div>
                <div class="year">{{ $movie->year }}</div>
            </div>
            <div class="movie-detail">
                <div class="label">Edition:</div>
                <div class="edition">{{ $movie->edition }}</div>
            </div>
            <div class="movie-detail">
                <div class="label">Plot:</div>
                <div class="plot">{{ $movie->description }}</div>
            </div>
            <h3>Actors</h3>
            <ul class="character-list">
            @foreach($movie->actors() as $actor):
                <li><a href="{{ route('actor', ['id' => $actor->id]) }}">{{ $actor->name }}</a></li>
            @endforeach
            </ul>
            <h3>Directors</h3>
            <ul class="character-list">
            @foreach($movie->directors as $director):
                <li><a href="{{ route('director', ['id' => $director->id]) }}">{{ $director->name }}</a></li>
            @endforeach
            </ul>
            <h3>Studios</h3>
            <ul class="character-list">
            @foreach($movie->studios as $studio):
                <li><a href="{{ route('studio', ['id' => $studio->id]) }}">{{ $studio->name }}</a></li>
            @endforeach
            </ul>
        </div>
    </div>
@endsection
