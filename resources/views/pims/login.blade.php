{% extends "pims/layout.html" %}

{% block body %}
    <div class="card">
        <div class="card-header">
            <h2>Login</h2>
        </div>
        <div class="card-body">
            {% if message %}
                <div>{{ message }}</div>
            {% endif %}
            <form action="{% url 'login' %}" method="post">
                {% csrf_token %}
                <div class="form-group">
                    <input autofocus class="form-control" type="text" name="username" placeholder="Username">
                </div>
                <div class="form-group">
                    <input class="form-control" type="password" name="password" placeholder="Password">
                </div>
                <input class="btn btn-primary" type="submit" value="Login">
            </form>
        </div>
    </div>

{% endblock %}