<!DOCTYPE html>
<html lang="en">
    <head>
        <title>@section('title')P.I.M.S.@show</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link href="/assets/css/styles.css" rel="stylesheet">
        <script src="/assets/js/site.js"></script>
    </head>
    <body>
        <div class="container">
        @include('pims.partials.nav', ['active'=>$active])
            <div class="row">
                <div class="col align-items-center">
                    @yield('body')
                </div>
            </div>
        </div>
    </body>
</html>
