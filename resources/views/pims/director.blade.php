{% extends 'pims/layout.html' %}

{% block body %}
    <h2>Director Details</h2>
    <h3 class="movie-title">{{ director.name }}</h3>

    <h3>Movies</h3>
    {% include 'pims/partials/movie_list.html' with movies=movies %}
{% endblock %}