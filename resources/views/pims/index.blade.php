@extends("pims.layout")

@section('body')
    <h2>Movie List</h2>
    @include('pims.partials.movie_list', ['movies'=>$movies])
@endsection
