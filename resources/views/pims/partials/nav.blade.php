<ul class="nav nav-pills nav-fill">
    <li class="nav-item">
        <a class="nav-link @if($active=='movies') active @endif" href="{{ route('movies') }}">Movies</a>
    </li>
    @if(!empty($authorized))
        <li class="nav-item">
            <a class="nav-link @if($active=='movies') active @endif" href="{{ route('add_movie') }}">Add Movie</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}">Logout</a>
        </li>
    @else
        <li class="nav-item">
            <a class="nav-link @if($active == 'login') active @endif" href="{{ route('login') }}">Login</a>
        </li>
    @endif
</ul>
