<div id="movie-list-results">
@foreach($movies->items() as $movie):
    <div class="movie-item">
        <a href='{{ route('movie', ['id'=>$movie->id])}}'>
            <img class="poster" src='{{ $movie->image }}' />
            <h3 class="title">{{ $movie->title }}</h3>
            <div class="year">{{ $movie->year }}</div>
        </a>
    </div>
@endforeach
</div>
<div class="pagination">
    <span class="step-links">
        <span class="current">
            Page {{ $movies->currentPage() }} of {{ $movies->lastPage() }}
        </span>
        @if(!$movies->onFirstPage()):
            <a class="movie-link btn btn-primary" href="?page={{ $movies->currentPage() - 1 }}">Prev</a>
        @else:
            <a class="movie-link btn btn-outline-primary disabled" aria-disabled="true" disabled>Prev</a>
        @endif

        @if($movies->hasMorePages()):
            <a class="movie-link btn btn-primary" href="?page={{ $movies->currentPage() + 1 }}">Next</a>
        @else:
            <a class="movie-link btn btn-outline-primary disabled" aria-disabled="true" disabled>Next</a>
        @endif:
    </span>
</div>
