{% extends "pims/layout.html" %}

{% block body %}
    <div class="card" id="movie-search-form">
        <div class="card-header">
            <h2>Add Movie</h2>
        </div>
        <div class="card-body">
            {% if message %}
                <div>{{ message }}</div>
            {% endif %}
            <form onsubmit="return false">
                {% csrf_token %}
                <div class="form-group">
                    <input autofocus class="form-control" id="movie-title-search" type="text" name="title" placeholder="Movie Title">
                </div>
                <button class="btn btn-primary" onclick="searchForMovie()">Search</button>
            </form>
        </div>
    </div>
    <div id="movie-search-results">
    </div>
    <div>FILE: {{ env }}</div>
    <div>OMDB: {{ omdb  }}</div>
{% endblock %}
