<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MovieController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MovieController::class, 'index'])->name('movies');
Route::get('/movies/{id}', [MovieController::class, 'single'])->name('movie');
Route::post('/movies', [MovieController::class, 'index'])->name('add_movie');
//
//Route::get('/actors/{id}', '');
//Route::get('/directors/{id}', '');
//Route::get('/studios/{id}', '');
//
Route::get('/login', [MovieController::class, 'index'])->name('login');
Route::get('/logout', [MovieController::class, 'index'])->name('logout');

